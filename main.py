import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import pandas as pd
import matplotlib
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)  # y labels are oh-encoded



import numpy as np
from PIL import Image


n_input = 784
n_nodes_hl1=500
n_nodes_hl2=500
n_nodes_hl3=500

n_classes=10
batch_size=100


# x height, y widht
x = tf.placeholder("float", [None,784])
y = tf.placeholder("float", )

def neural_network_model(data):

    # (input data*weights)+biases
    hidden_1_layer={'weights':tf.Variable(tf.random_normal([784,n_nodes_hl1])),
                    'biases':tf.Variable(tf.random_normal([n_nodes_hl1]))}
    hidden_2_layer={'weights':tf.Variable(tf.random_normal([n_nodes_hl1,n_nodes_hl2])),
                    'biases':tf.Variable(tf.random_normal([n_nodes_hl2]))}
    hidden_3_layer={'weights':tf.Variable(tf.random_normal([n_nodes_hl2,n_nodes_hl3])),
                    'biases':tf.Variable(tf.random_normal([n_nodes_hl3]))}
    output_layer={'weights':tf.Variable(tf.random_normal([n_nodes_hl3,n_classes])),
                    'biases':tf.Variable(tf.random_normal([n_classes]))}

    l1=tf.add(tf.matmul(data,hidden_1_layer['weights']),hidden_1_layer['biases'])
    l1=tf.nn.relu(l1)

    l2=tf.add(tf.matmul(l1,hidden_2_layer['weights']),hidden_2_layer['biases'])
    l2=tf.nn.relu(l2)

    l3=tf.add(tf.matmul(l2,hidden_3_layer['weights']),hidden_3_layer['biases'])
    l3=tf.nn.relu(l3)

    output=tf.matmul(l3,output_layer['weights'])+output_layer['biases']

    return output

def train_neural_network(x):
    prediction = neural_network_model(x)

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=prediction))
                                    #(): Accuracy: 0.9488
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    #hm_epochs = 10
    hm_epochs = 20

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    for epoch in range(hm_epochs):
        epoch_loss = 0
        for _ in range(int(mnist.train.num_examples/batch_size)):
            epoch_x,epoch_y = mnist.train.next_batch(batch_size)
            _, c = sess.run([optimizer,cost],feed_dict={x:epoch_x,y:epoch_y})
            epoch_loss +=c
        print('Epoch',epoch,'completed out of',hm_epochs,'loss:',epoch_loss)
        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))
        print(accuracy.eval({x: mnist.test.images, y: mnist.test.labels}, session=sess))

    return prediction, sess

def print_histogram(labels, filename):
    hist = pd.Series(labels)
    vc = hist.value_counts().sort_index()
    ax = vc.plot(kind='bar')

    fig = ax.get_figure()
    fig.autofmt_xdate()
    fig.savefig(filename)


train_labels=[]
for labels in mnist.train.labels:
    train_labels.append(np.squeeze(np.where(labels==1)))
print_histogram(train_labels,'emnist.png')


trained, sess=train_neural_network(x)
print(trained)

#img = np.invert(Image.open("test_img.png").convert('L')).ravel()/255
img = np.invert(Image.open("test_img.png").convert('L')).ravel()
prediction = sess.run(trained, feed_dict={x: [img]})
print (prediction)
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image2:", prediction)

img = np.invert(Image.open("test_img0.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image0:", np.squeeze(prediction))

img = np.invert(Image.open("test_img1.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image1:", np.squeeze(prediction))

img = np.invert(Image.open("test_img3.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image3:", np.squeeze(prediction))

img = np.invert(Image.open("test_img4.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image4:", np.squeeze(prediction))

img = np.invert(Image.open("test_img6.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image6:", np.squeeze(prediction))

img = np.invert(Image.open("test_img7.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image7:", np.squeeze(prediction))

img = np.invert(Image.open("test_img8.png").convert('L')).ravel()
prediction = sess.run(tf.argmax(trained, 1), feed_dict={x: [img]})
print("Prediction for test image8:", np.squeeze(prediction))


sess.close()